export default {
  components: true,
  head: {
    titleTemplate: 'Mastring Nuxt: %s',
    htmlAttrs: {
      lang: 'en',
    },
    bodyAttrs: {
      class: ['my-style'],
    },
    meta: [{ charset: 'utf-8' }],
    link: [
      {
        rel: 'icon',
        type: 'image/x-icon',
        href: '/favicon.png',
      },
    ],
  },
  router: {
    prefetchLinks: false,
  },
  plugins: [
    '~/plugins/maps.client',
    '~/plugins/dataApi',
    '~/plugins/auth.client',
  ],
  modules: ['~/modules/auth', '~/modules/algolia'],
  buildModules: ['@nuxtjs/tailwindcss'],
  serverMiddleware: [],
  css: ['~/assets/sass/app.scss'],
  build: {
    extractCSS: true,
    loaders: {
      limit: 0,
    },
  },
  publicRuntimeConfig: {
    auth: {
      cookieName: 'idToken',
      clientId: process.env.GOOGLE_CLIENT_ID,
    },
    google: {
      apiKey: process.env.GOOGLE_API_KEY || 'googleApiKey',
    },
    algolia: {
      appId: process.env.ALGOLIA_APP_ID || 'algoliaAppId',
      key: process.env.ALGOLIA_API_KEY || 'algoliaKey',
    },
  },
  privateRuntimeConfig: {
    algolia: {
      appId: process.env.ALGOLIA_APP_ID || 'algoliaAppId',
      key: process.env.ALGOLIA_API_ADMIN_KEY || 'algoliaKey',
    },
  },
}
