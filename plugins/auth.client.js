import Cookie from 'js-cookie'
import { unWrap } from '~/utils/fetchUtils'

export default ({ $config, store }, inject) => {
  window.onGoogleLibraryLoad = init

  addScript()

  inject('auth', {
    signOut,
  })

  function addScript() {
    const script = document.createElement('script')
    script.src = 'https://accounts.google.com/gsi/client'
    script.async = true
    script.defer = true
    document.head.appendChild(script)
  }

  async function init() {
    const idToken = Cookie.get($config.auth.cookieName)
    if (idToken) {
      await logIn(idToken)
      return
    }

    window.google.accounts.id.initialize({
      client_id: $config.auth.clientId,
      callback: parseUser,
      auto_select: true,
    })
    window.google.accounts.id.renderButton(
      document.getElementById('googleButton'),
      { theme: 'outline', size: 'large' }
    )
    window.google.accounts.id.prompt()
  }

  function parseUser(response) {
    logIn(response.credential)
  }

  async function logIn(idToken) {
    Cookie.set($config.auth.cookieName, idToken, {
      expires: 1 / 24,
      sameSite: 'lax',
    })

    try {
      const response = await unWrap(await fetch('/api/user'))
      const user = response.json

      store.commit('auth/user', {
        fullName: user.name,
        profileUrl: user.image,
      })
    } catch (error) {
      console.error(error)
    }
  }

  function signOut() {
    window.google.accounts.id.disableAutoSelect()
    store.commit('auth/user', null)
    Cookie.remove($config.auth.cookieName)
  }
}
